<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InsertContoller;
use App\Http\Controllers\GetdataController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/admin', function () {
    return view('admin');
});
Route::get('/', function () {
    return view('admin1');
})->name('admin1');
Route::get('/login', function () {
	if(Auth::guest()){
    return view('login');
}else{
	 return redirect(route('admin1'));	
}
});
Route::get('404', function () {
    return view('404');
});
Route::get('/register', function () {
    return view('register');
});
Route::get('/forgot-password', function () {
    if(Auth::guest()){
    return view('forgot-password');
}else{
     return redirect(route('admin1'));  
}
});
Route::get('/blank', function () {
    return view('blank');
});
Route::get('/utilities-color', function () {
    return view('utilities-color');
});
Route::get('/utilities-border', function () {
    return view('utilities-border');
});
Route::get('/utilities-animation', function () {
    return view('utilities-animation');
});
Route::get('/utilities-other', function () {
    return view('utilities-other');
});
Route::get('/charts', function () {
    return view('charts');
});
// Route::get('/tables', function () {
//     return view('tables');
// });
Route::get('/buttons', function () {
    return view('buttons');
});
Route::get('/cards', function () {
    return view('cards');
});
Route::get('/cards', function () {
    return view('cards');
});
Route::get('/reset_password', function () {
    return view('reset_password');
})->name('reset_password');
Route::post('/insert', [InsertContoller::class, 'create'])->name('insert');
Route::get('/tables', [GetdataController::class, 'show'])->name('tables');
Route::any('/userlogin', [LoginController::class, 'authenticate'])->name('userlogin');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/delete/{id}', [GetdataController::class, 'delete'])->name('delete');
Route::get('/edit/{id}', [GetdataController::class, 'edit'])->name('edit');
Route::any('/update/{id}', [GetdataController::class, 'update'])->name('update');
Route::any('/sendmail',[MailController::class, 'basic_email'])->name('sendmail');
Route::any('/verify',[MailController::class, 'otp_verify'])->name('otp_verify');
Route::any('/change_password',[MailController::class, 'change_password'])->name('change_password');
