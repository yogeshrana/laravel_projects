<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});

Route::post('/add_address', [UserController::class,'add_address']);
Route::post('/add_car', [UserController::class,'add_car']);
Route::post('/nickname', [UserController::class,'nickname']);
Route::post('/userdata', [UserController::class,'register']);
Route::post('/login', [UserController::class,'login']);
Route::get('/getuserdata', [UserController::class,'getuserdata']);

Route::middleware('auth:api')->get('/userdetail', [UserController::class,'details']);
