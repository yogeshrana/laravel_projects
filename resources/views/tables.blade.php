@include('includes.header')

    <!-- Sidebar -->
   @include('includes.sidebar')
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
   @include('includes.header2')
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <style type="text/css">
          svg{
            display: none;
          }
        </style>
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Tables</h1>
          <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Email</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>User Name</th>
                      <th>Created Time</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>ID</th>
                      <th>Email</th>
                      <th>First Name</th>
                      <th>Last Nam</th>
                      <th>User Name</th>
                      <th>Created Time</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    
                     @foreach ($users as $user)
                    <tr>
                      <td>{{$user->id}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->fname}}</td>
                      <td>{{$user->lname}}</td>
                      <td>{{$user->name}}</td>
                      <td>{{$user->created_at}}</td>
                      <!-- data-toggle="modal" data-target="#editmodel" -->
                      <td><a  href="{{route('edit',$user->id)}}" >Edit</a>|<a href="{{route('delete', $user->id)}}">Delete</a></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <nav aria-label="Page navigation example">
                <ul class="pagination" style="">
                  {{$users->appends(request()->input())->links()}}
                </ul>
              </nav>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
 <!--  <div class="modal fade" id="editmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Update?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        @if (count($errors) > 0)
              <div class = "alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                  </ul>
              </div>
            @endif
        <form class="user" method="post" action="{{route('update',$user->id)}}">
           @csrf
          <div class="container" style="margin-top: 10px">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" name="fname" value="{{$user->fname}}" class="form-control form-control-user" id="exampleFirstName" placeholder="First Name">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" name="lname" value="{{$user->lname}}" class="form-control form-control-user" id="exampleLastName" placeholder="Last Name">
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" name="email" value="{{$user->email}}" class="form-control form-control-user" id="exampleInputEmail" placeholder="Email Address">
                </div>
                 </div>
             
             
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary" >Update</button>
        </div>
         </form>
      </div>
    </div>
  </div> -->
      <!-- Footer -->
    @include('includes.footer')