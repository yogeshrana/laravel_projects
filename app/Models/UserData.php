<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    use HasFactory;
     protected $guarded=['id'];

     public function getcar(){

     	return $this->hasMany('App\Models\AddCar','user_id');

     }
     public function getaddress(){

     	return $this->hasMany('App\Models\Useraddress','user_id');

     }
     public function nickname(){

        return $this->hasOne('App\Models\Nickname','address_id');

    }
}
