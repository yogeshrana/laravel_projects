<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class LoginController extends Controller
{
     public function authenticate(Request $request)
    {
    	 $this->validate(request(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);	

    	 if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

   			 return view('admin1');
		        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    
    }

     public function logout(Request $request)
    {
    	Auth::logout();
  		return redirect('/login');
    }
}
