<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Useraddress;
use App\Models\Nickname;
use App\Models\AddCar;
use App\Models\User;
use Auth;
use Hash;


class UserController extends Controller
{
    public function add_address(Request $request){

    	$address = Useraddress::create([
    		'user_id'=>$request->user_id,
    		'street_no'=>$request->street_no,
    		'house_no'=>$request->house_no,
    		'address'=>$request->address,
    	]);

    	if ($address) {
    		return response()->json(['Status'=>'1','message'=>'Address inserted Successfull']);
    	}

    }
    public function nickname(Request $request){
    	$nickname = Nickname::create([
    		'address_id'=>$request->address_id,
    		'address_nickname'=>$request->nickname,

    	]);
    if ($nickname) {
    		return response()->json(['Status'=>'1','message'=>'Nickname inserted Successfull']);
    	}

    }
    public function add_car(Request $request){

		$add_car = AddCar::create([
    		'user_id'=>$request->user_id,
    		'car_name'=>$request->car_name,

    	]);
    	if ($add_car) {
    		return response()->json(['Status'=>'1','message'=>'Car inserted Successfull']);
    	}

    }
   public function register(Request $request){
		$UserData = User::create([
			
    		'fname'=>$request->first_name,
    		'lname'=>$request->last_name??'',
    		'name'=>$request->first_name.' '.$request->last_name,
    		'email'=>$request->email,
    		'phone'=>$request->phone,
    		'password'=>Hash::make($request->password)

		]);
		// $success['token'] =  $UserData->createToken()->accessToken;
    	if ($UserData) {
    		return response()->json(['Status'=>'1','message'=>'User Data inserted Successfull']);
    	}

	}
	
	public function login(Request $request){

		if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {


			$user = Auth::user();
			
			$success['token'] =  $user->createToken('MyAppYogesh')->accessToken; 


				return response()->json(['Status'=>'1','message'=>'User Login Successfull','token'=>$success['token']]);
			}else{
				return response()->json(['Status'=>'0','message'=>'password wrong']);
			}

	}

    public function getuserdata(){

    	$data=User::orderby('id','ASC')->with('getaddress:id,user_id,street_no,house_no,address','getcar:id,car_name,user_id','nickname:id,address_id,address_nickname')->get()->makeHidden(['created_at','updated_at']);

    	return response()->json(['status'=>'1','message'=>'data fetch successfull','data'=>$data]);

	}
	public function details() 
    { 
        $user = Auth::user()->makeHidden('created_at','updated_at'); 
        return response()->json(['success' => $user], 200); 
    } 
}
