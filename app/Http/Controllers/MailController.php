<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use Hash;
use Session;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\TestNotification;
use Illuminate\Support\Facades\Auth;


class MailController extends Controller
{		

   public function basic_email(Request $request) {
      $otp=rand(1000,9999);
      $url=route('reset_password').'?email='.$request->email;
      $user = User::where('email',$request->email)->first();
      if ($user!=null) {
         $user1=User::where('email',$request->email)->update([
          'remember_token' => $request->_token,
          'otp' => $otp,
       ]);
         $user->notify(new TestNotification($url,$otp));
         Session::flash('alert-class', 'alert-info'); 
         return back()->with('message', 'Email send successfull!');
         
      }
      else{
         Session::flash('alert-class', 'alert-danger'); 
         return back()->with('message', 'This email is not registered!');
         
     }
  }
  public function otp_verify(Request $request) {

   $user = User::where(['email'=>$request->email,'otp'=>$request->otp])->first();

   if ($user) {
      Session::flash('alert-class', 'alert-info'); 
    return view('set_password',['message'=> 'Your OTP is Verify Successful','email'=>$request->email]);
                           
 }else{
   Session::flash('alert-class', 'alert-danger'); 
         return back()->with('message', 'OTP is wrong!');
}
}
public function change_password(Request $request) {

   if ($request->password==$request->cpassword) {
     $user=User::where('email',$request->email)->update([
       'password' => Hash::make($request->password),
    ]);
     return view('admin1');
  }else{
   Session::flash('alert-class', 'alert-danger'); 
         return back()->with('message', 'confirm password does not match!');
}

}

}
