<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class GetdataController extends Controller
{
	
    public function show(Request $request)
	{
		$users = User::where('id','!=',NULL);
		if (isset($request->name)) {
			$users =$users->where('name','LIKE','%'.$request->name.'%');
		}
		$users =$users->paginate(3);
		return view('tables')->with('users', $users);
	}

	public function delete($id)
	{
	    User::destroy($id);
	    return back();
	}
	public function edit($id)
	{
	    $user=User::find($id);

	     return view('edit')->with('user', $user);
	    // return $user;
	}

	public function update(Request $request,$id)
	{
		  $validator = Validator::make($request->all(), [
            'name' => 'required|max:10',
            'email' => 'required|email|'. Rule::unique('users')->ignore($id),
        ]);
		return back()->withErrors([
            'email' => 'This is Duplicate email',
        ]);
	    $user=User::where('id',$id)->update([
           'fname' => $request->fname,
           'lname' => $request->lname,
           'email' => $request->email,
           'name' => $request->fname.' '.$request->lname
        ]);
	    
	    return back();
	    
	}
}
