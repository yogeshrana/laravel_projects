<?php

namespace App\Http\Controllers;
use Hash;
use Illuminate\Http\Request;
use App\Models\User;
class InsertContoller extends Controller
{
	 function create(Request $req)
	{
		
		 $this->validate(request(), [
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);

	    $user = new User;
	    $user->fname = $req->fname;
	    $user->lname = $req->lname;
	    $user->name = $req->fname.' '.$req->lname;
	    $user->email = $req->email;
	    $user->password =Hash::make($req->password);
	    $user->email_verified_at =time();
	    $user->remember_token =$req->input()['_token'];
	    $user->save();

	    return view('login');
	 }

}
